#pragma once
#include <opencv2/opencv.hpp>
#include <cstdlib>
#include <iostream>
#include <windows.h>
#include <string>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>

using namespace std;
using namespace cv;

class Thresholding
{
public:
	void thresColorImg(Mat &image);

private:
	void EroDil(Mat &image);
};

