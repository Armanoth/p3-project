#pragma once
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include"opencv2/imgproc/imgproc.hpp"
#include <iostream>
#define _USE_MATH_DEFINES
#include<math.h>

using namespace std;
using namespace cv;

class Camera
{
public:
	void getImg(Mat &input,VideoCapture cap);
	
private:
	void backwardsMapping(Mat &image);

};

