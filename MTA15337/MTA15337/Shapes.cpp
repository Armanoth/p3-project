#include "Shapes.h"

using namespace std;
using namespace cv;

int Shapes::getWidth(int x1, int x2)
{
	int width = x2 - x1;

	if (width < 0)
		width = 0;

	return width;
}

int Shapes::getW(int x1, int x2)
{
	int width = x2 - x1;

	if (width < 0)
		width = 0;

	//Normalise values "(W/200) * 100"
	width = width / 2;
	return width;
}

int Shapes::getH(int y1, int y2)
{
	int height = y2 - y1;

	if (height < 0)
		height = 0;

	//Normalise values "(H/200) * 100"
	height = height / 2;
	return height;
}

int Shapes::getP(Mat shape)
{
	Mat image2;
	image2 = shape.clone();
	int whitePixels = 0;

	if (shape.data && !image2.empty()){
		for (size_t y = 1; y < shape.rows - 1; ++y){
			for (size_t x = 1; x < shape.cols - 1; ++x){
				int midPixel = shape.at<unsigned char>(y, x);//Current pixel
				int topMid = shape.at<unsigned char>(y - 1, x);//pixel above current pixel
				int midLeft = shape.at<unsigned char>(y, x - 1);//pixel left of current pixel
				int midRight = shape.at<unsigned char>(y, x + 1);//pixel right of current pixel
				int botMid = shape.at<unsigned char>(y + 1, x);//pixel below of current pixel

				if (midPixel == 255){
					if (topMid == 0 || midRight == 0 || botMid == 0 || midLeft == 0){
						image2.at<uchar>(y, x) = 255;
						++whitePixels;
					}
					else{
						image2.at<uchar>(y, x) = 0;
					}
				}


			}//xLoop ENDS
		}//yLoop ENDS
	}

	//Normalise value "(P/800) * 100"
	whitePixels = whitePixels / 8;
	return whitePixels;
}

float Shapes::getC(float perimeter, int area)
{
	float c = perimeter / (2 * sqrt(M_PI * area));

	//Normalise value "(C/2) * 100"
	c = c / 0.2;
		return c;
}

float Shapes::getR(int x, int y)
{
	if (x < y)
	{
		swap(x, y);
	}

	double ratio = (x / (double)y);

	//Normalise value "(R /5) * 100"
	ratio = ratio / 0.05; 

	return ratio;
}

int Shapes::getA(Mat blob)
{
	int pixelCount = 0;
	if (blob.data && !blob.empty())
	{
		int countR = 0, countG = 0, countB = 0;
		for (size_t y = 0; y < blob.rows; ++y)
		{
			for (size_t x = 0; x < blob.cols; ++x)
			{
				int pixelValue = blob.at<uchar>(y, x);

				if (pixelValue > 0)
				{
					pixelCount++;
				}
				
			}
		}
	}
	pixelCount = pixelCount / 260;
	return pixelCount;
}

float Shapes::getVectorDistance(vector<sixD> vector1, vector<sixD> vector2)
{
	int w1 = vector1[0].w;
	int w2 = vector2[0].w;
	int h1 = vector1[0].h;
	int h2 = vector2[0].h;
	int a1 = vector1[0].a;
	int a2 = vector2[0].a;
	int p1 = vector1[0].p;
	int p2 = vector2[0].p;
	int c1 = vector1[0].c;
	int c2 = vector2[0].c;
	int r1 = vector1[0].r;
	int r2 = vector2[0].r;

	float distance = sqrt(pow(w1 - w2, 2) + pow(h1 - h2, 2) + pow(a1 - a2, 2) + pow(p1 - p2, 2) + pow(c1 - c2, 2) + pow(r1 - r2, 2));
	
	return distance;
}

Mat Shapes::isolateBlob(Mat &image, int i){

	Mat isolatedBlob = image.clone();
	Mat blob = image.clone();
	int pixelVal;
	int x1 = image.cols;
	int x2 = 0;
	int y1 = image.rows;
	int y2 = 0;
	int extraPixels = 6; //extra pixels for boundrybox

	//Go through all the pixels, and highlight the pixel value we are looking for (i)
	for (size_t y = 0; y < blob.rows; ++y)
	{
		for (size_t x = 0; x < blob.cols; ++x)
		{
			if (image.at<uchar>(y, x) == i)
			{
				blob.at<uchar>(y, x) = 255;
			}
			else
			{
				blob.at<uchar>(y, x) = 0;
			}
		}
	}
	
	//Find the isolated shapes boundry box
	for (size_t y = 0; y < blob.rows; ++y)
	{
		for (size_t x = 0; x < blob.cols; ++x)
		{
			if (blob.at<uchar>(y, x) > 0)
			{
				if (x < x1)
					x1 = x;

				if (x > x2)
					x2 = x;

				if (y < y1)
					y1 = y;

				if (y > y2)
					y2 = y;
			}
		}
	}

	isolatedBlob.cols = getWidth(x1, x2) + extraPixels;
	isolatedBlob.rows = getWidth(y1, y2) + extraPixels;
	for (size_t y = extraPixels/2; y < isolatedBlob.rows - (extraPixels / 2); ++y)
	{
		for (size_t x = extraPixels/2; x < isolatedBlob.cols - (extraPixels / 2); ++x)
		{
			if (x2 > x1 && y2 > y1 && y + y1 < image.rows && x + x1 < image.cols)
			{
				if (image.at<uchar>(y + y1, x + x1) > 0)
				{
					isolatedBlob.at<uchar>(y, x) = 255;

				}
			}
		}
	}
	return isolatedBlob;
}

int Shapes::findBlob(Mat &Img){
	Mat image = Img.clone();
	Mat blobs = image.clone();
	int label = 0;
	int pluslabel = 1;

	//Sets all the pixels on the new pixture (blobs) to black
	for (size_t y = 0; y < image.rows; ++y) {
		for (size_t x = 0; x < image.cols; ++x) {
			blobs.at<uchar>(y, x) = 0;
		}
	}

	//Forwards loop
	for (size_t y = 1; y < image.rows; ++y) {
		for (size_t x = 1; x < image.cols; ++x) {
			//Makes the label if the top and left is black and the mid is white
			if (image.at<uchar>(y - 1, x) == 0 && image.at<uchar>(y, x - 1) == 0 && image.at<uchar>(y, x) > 0){ //Topinput, left input and mid
				label = label + pluslabel;
				blobs.at<uchar>(y, x) = label;
			}
			//Take top pixel if larger than zero
			if (image.at<uchar>(y - 1, x) > 0)
				blobs.at<uchar>(y, x) = blobs.at<uchar>(y - 1, x);
			//Take left pixel if larger than zero
			if (image.at<uchar>(y, x - 1) > 0)
				blobs.at<uchar>(y, x) = blobs.at<uchar>(y, x - 1);

			if (blobs.at<uchar>(y - 1, x) < blobs.at<uchar>(y, x) && blobs.at<uchar>(y - 1, x) != 0)
				blobs.at<uchar>(y - 1, x) = blobs.at<uchar>(y, x);

			if (blobs.at<uchar>(y, x - 1) < blobs.at<uchar>(y, x) && blobs.at<uchar>(y, x - 1) != 0)
				blobs.at<uchar>(y, x - 1) = blobs.at<uchar>(y, x);
		}//End of for loop
	}//End of for loop

	//Backwards loop, 1st run through to fix major bugs
	for (size_t y = blobs.rows - 1; y > 1; --y) {
		for (size_t x = blobs.cols - 1; x > 1; --x) {
			if (blobs.at<uchar>(y, x) != 0){ // If the pixel is white

				if (blobs.at<uchar>(y + 1, x) < blobs.at<uchar>(y, x) && blobs.at<uchar>(y + 1, x) != 0)
					blobs.at<uchar>(y + 1, x) = blobs.at<uchar>(y, x);

				if (blobs.at<uchar>(y - 1, x) < blobs.at<uchar>(y, x) && blobs.at<uchar>(y - 1, x) != 0)
					blobs.at<uchar>(y - 1, x) = blobs.at<uchar>(y, x);

				if (blobs.at<uchar>(y, x - 1) < blobs.at<uchar>(y, x) && blobs.at<uchar>(y, x - 1) != 0)
					blobs.at<uchar>(y, x - 1) = blobs.at<uchar>(y, x);

				if (blobs.at<uchar>(y , x + 1) < blobs.at<uchar>(y, x) && blobs.at<uchar>(y, x + 1) != 0)
					blobs.at<uchar>(y , x + 1) = blobs.at<uchar>(y, x);
			}
		}//End of for loop
	}//End of for loop

	image = blobs;
	Img = image.clone();

	return label;
}

int Shapes::compareShape(vector<struct sixD> input)    
{
	int shapeType = 0;
	const int snums = 6;
	sixD newD;

	vector<sixD> shape1, shape2, shape3, shape4, shape5, shape6;
	shape1.push_back(newD);
	shape2.push_back(newD);
	shape3.push_back(newD);
	shape4.push_back(newD);
	shape5.push_back(newD);
	shape6.push_back(newD);

	//Starting point
	int x1 = 0;
	int y1 = 0;
	//Define our ideal shapes values and compare distance.
	Mat S1 = imread("shapeT1.png", 0);
	int S1x2 = S1.cols;
	int S1y2 = S1.rows;
	shape1[0].w = getW(x1, S1x2);
	shape1[0].h = getH(y1, S1y2);
	shape1[0].a = getA(S1);
	shape1[0].p = getP(S1);
	shape1[0].c = getC(shape1[0].p, shape1[0].a);
	shape1[0].r = getR(shape1[0].w, shape1[0].h);
	int shape1distance = getVectorDistance(input, shape1);

	Mat S2 = imread("shapeT2.png", 0); 
	int S2x2 = S2.cols;
	int S2y2 = S2.rows;
	shape2[0].w = getW(x1, S2x2);
	shape2[0].h = getH(y1, S2y2);
	shape2[0].a = getA(S2);
	shape2[0].p = getP(S2);
	shape2[0].c = getC(shape2[0].p, shape2[0].a);
	shape2[0].r = getR(shape2[0].w, shape2[0].h);
	int shape2distance = round(getVectorDistance(input, shape2));

	Mat S3 = imread("shapeT3.png", 0);
	int S3x2 = S3.cols;
	int S3y2 = S3.rows;
	shape3[0].w = getW(x1, S3x2);
	shape3[0].h = getH(y1, S3y2);
	shape3[0].a = getA(S3);
	shape3[0].p = getP(S3);
	shape3[0].c = getC(shape3[0].p, shape3[0].a);
	shape3[0].r = getR(shape3[0].w, shape3[0].h);
	int shape3distance = round(getVectorDistance(input, shape3));

	Mat S4 = imread("shapeT4.png", 0);
	int S4x2 = S4.cols;
	int S4y2 = S4.rows;
	shape4[0].w = getW(x1, S4x2);
	shape4[0].h = getH(y1, S4y2);
	shape4[0].a = getA(S4);
	shape4[0].p = getP(S4);
	shape4[0].c = getC(shape4[0].p, shape4[0].a);
	shape4[0].r = getR(shape4[0].w, shape4[0].h);
	int shape4distance = round(getVectorDistance(input, shape4));

	Mat S5 = imread("shapeT5.png", 0); 
	int S5x2 = S5.cols;
	int S5y2 = S5.rows;
	shape5[0].w = getW(x1, S5x2);
	shape5[0].h = getH(y1, S5y2);
	shape5[0].a = getA(S5);
	shape5[0].p = getP(S5);
	shape5[0].c = getC(shape5[0].p, shape5[0].a);
	shape5[0].r = getR(shape5[0].w, shape5[0].h);
	int shape5distance = round(getVectorDistance(input, shape5));

	Mat S6 = imread("shapeT6.png", 0); // CHANGE
	int S6x2 = S6.cols;
	int S6y2 = S6.rows;
	shape6[0].w = getW(x1, S6x2);
	shape6[0].h = getH(y1, S6y2);
	shape6[0].a = getA(S6);
	shape6[0].p = getP(S6);
	shape6[0].c = getC(shape6[0].p, shape6[0].a);
	shape6[0].r = getR(shape6[0].w, shape6[0].h);
	int shape6distance = round(getVectorDistance(input, shape6));

	//WRITE FILE
	ofstream dbugsheet;
	dbugsheet.open("reference shapes.txt");
	dbugsheet << "shape1 W: " << shape1[0].w << endl;
	dbugsheet << "shape1 H: " << shape1[0].h << endl;
	dbugsheet << "shape1 A: " << shape1[0].a << endl;
	dbugsheet << "shape1 P: " << shape1[0].p << endl;
	dbugsheet << "shape1 C: " << shape1[0].c << endl;
	dbugsheet << "shape1 R: " << shape1[0].r << endl;
	dbugsheet << endl;
	dbugsheet << "shape2 W: " << shape2[0].w << endl;
	dbugsheet << "shape2 H: " << shape2[0].h << endl;
	dbugsheet << "shape2 A: " << shape2[0].a << endl;
	dbugsheet << "shape2 P: " << shape2[0].p << endl;
	dbugsheet << "shape2 C: " << shape2[0].c << endl;
	dbugsheet << "shape2 R: " << shape2[0].r << endl;
	dbugsheet << endl;
	dbugsheet << "shape3 W: " << shape3[0].w << endl;
	dbugsheet << "shape3 H: " << shape3[0].h << endl;
	dbugsheet << "shape3 A: " << shape3[0].a << endl;
	dbugsheet << "shape3 P: " << shape3[0].p << endl;
	dbugsheet << "shape3 C: " << shape3[0].c << endl;
	dbugsheet << "shape3 R: " << shape3[0].r << endl;
	dbugsheet << endl;
	dbugsheet << "shape4 W: " << shape4[0].w << endl;
	dbugsheet << "shape4 H: " << shape4[0].h << endl;
	dbugsheet << "shape4 A: " << shape4[0].a << endl;
	dbugsheet << "shape4 P: " << shape4[0].p << endl;
	dbugsheet << "shape4 C: " << shape4[0].c << endl;
	dbugsheet << "shape4 R: " << shape4[0].r << endl;
	dbugsheet << endl;
	dbugsheet << "shape5 W: " << shape5[0].w << endl;
	dbugsheet << "shape5 H: " << shape5[0].h << endl;
	dbugsheet << "shape5 A: " << shape5[0].a << endl;
	dbugsheet << "shape5 P: " << shape5[0].p << endl;
	dbugsheet << "shape5 C: " << shape5[0].c << endl;
	dbugsheet << "shape5 R: " << shape5[0].r << endl;
	dbugsheet << endl;
	dbugsheet << "shape6 W: " << shape6[0].w << endl;
	dbugsheet << "shape6 H: " << shape6[0].h << endl;
	dbugsheet << "shape6 A: " << shape6[0].a << endl;
	dbugsheet << "shape6 P: " << shape6[0].p << endl;
	dbugsheet << "shape6 C: " << shape6[0].c << endl;
	dbugsheet << "shape6 R: " << shape6[0].r << endl;
	dbugsheet.close();

	int findLowestDistance[snums] = { shape1distance, shape2distance, shape3distance, shape4distance, shape5distance, shape6distance };
	bstNode* root = NULL;

	// Fills the binary search tree with the values from the array
	for (int i = 0; i < snums; i++){
		root = insert(root, findLowestDistance[i]);
	}

	// Prints the lowest number in the tree
	int mindistance = findMin(root);

	//IF LIMIT
	if (mindistance < 40)
	{
		for (int j = 0; j < snums; j++)
		{
			if (findLowestDistance[j] == findMin(root))
			{
				shapeType = j + 1;
				cout << "found shape:" << shapeType << endl;
				return shapeType;
			}
			else
			{
				continue;
			}
		}
	}
	return 0;
}

bstNode* Shapes::getNewNode(int data){
	//Create note
	bstNode* newNode = new bstNode();
	newNode->data = data;
	newNode->left = newNode->right = NULL;
	return newNode;
}

bstNode* Shapes::insert(bstNode* root, int data){
	// Inserting nodes into the tree
	if (root == NULL){
		root = getNewNode(data);
	}
	else if (data <= root->data){
		root->left = insert(root->left, data);
	}
	else{
		root->right = insert(root->right, data);
	}
	return root;
}

bool Shapes::search(bstNode* root, int data){
	// search function, if we want to find a specific number within the tree
	if (root == NULL) return false;
	else if (root->data == data) return true;
	else if (data <= root->data) return search(root->left, data);
	else return search(root->right, data);
}

int Shapes::findMin(bstNode* root){

	// Function for find the lowest number, by moving down the trees left nodes
	// If the tree is empty return error message
	if (root == NULL){
		cout << "Error\n";
		return 0;
	}

	// Loop for moving with the left nodes
	while (root->left != NULL){
		root = root->left;
	}
	return root->data;
}

void Shapes::identifyShape(Mat &thresImg)
{
	sixD newD;
	vector<sixD> inputShape;
	inputShape.push_back(newD);
	Mat image;
	Mat blob;
	Mat visualfeedback = imread("BG.jpg", 1);
	Mat sampleOne = imread("VisualOutput_1.png", 1);
	Mat sampleTwo = imread("VisualOutput_2.png", 1);
	Mat sampleThree = imread("VisualOutput_3.png", 1);
	Mat sampleFour = imread("VisualOutput_4.png", 1);
	Mat sampleFive = imread("VisualOutput_5.png", 1);
	Mat sampleSix = imread("VisualOutput_6.png", 1);
	ofstream myfile;
	int layers;
	int label;
	int foundShapes [10];
	int foundShapesXpos[10];
	int foundShapesYpos[10];
	int j = 0;


	image = thresImg.clone();
	label = findBlob(image); 
	thresImg = image.clone();

	myfile.open("inputshape.txt");
	myfile << "DETECTED SHAPES\n";

	for (int i = 0; i < 10; ++i)
	{
		foundShapes[i] = 0;
		foundShapesXpos[i] = 0;
		foundShapesYpos[i] = 0;
	}

	for (int i = 1; i < label + 1; ++i)
	{
		blob = isolateBlob(thresImg, i);
		int x1 = image.cols;
		int x2 = 0;
		int y1 = image.rows;
		int y2 = 0;

		for (size_t y = 0; y < image.rows; ++y)
		{
			for (size_t x = 0; x < image.cols; ++x)
			{
				if (image.at<uchar>(y, x) == i)
				{
					if (x < x1)
						x1 = x;

					if (x > x2)
						x2 = x;

					if (y < y1)
						y1 = y;

					if (y > y2)
						y2 = y;
				}
			}
		}

		if (blob.cols > 40 && blob.rows > 40)
		{
			inputShape[0].w = getW(x1,x2);
			inputShape[0].h = getH(y1,y2);
			inputShape[0].a = getA(blob);
			inputShape[0].p = getP(blob);
			inputShape[0].c = getC(inputShape[0].p, inputShape[0].a);
			inputShape[0].r = getR(inputShape[0].w, inputShape[0].h);
			int foundshape = compareShape(inputShape);
			foundShapes[j] = foundshape;
			foundShapesXpos[j] = x1;
			foundShapesYpos[j] = y1;
			j = j + 1;

			myfile.open("inputshape.txt", ios_base::app);
			myfile << endl;
			myfile << "FOUND SHAPE\n";
			myfile << "Input shape W: " << inputShape[0].w << endl;
			myfile << "Input shape H: " << inputShape[0].h << endl;
			myfile << "Input shape A: " << inputShape[0].a << endl;
			myfile << "Input shape P: " << inputShape[0].p << endl;
			myfile << "Input shape C: " << inputShape[0].c << endl;
			myfile << "Input shape R: " << inputShape[0].r << endl;
			myfile << "Shape Label: " << label << endl;
			myfile << "Shape type:" << foundshape << endl;
			myfile.close();
		}
	}
	int xPos, yPos;
	for (int i = 0; i < 10; ++i)
	{
		int shapenumber = foundShapes[i];
		xPos = foundShapesXpos[i];
		yPos = foundShapesYpos[i];
		Mat processed;

		if (shapenumber != 0)
		{
			switch (shapenumber) {
			case 1:
				processed = sampleOne.clone();
				break;
			case 2:
				processed = sampleTwo.clone();
				break;
			case 3:
				processed = sampleThree.clone();
				break;
			case 4:
				processed = sampleFour.clone();
				break;
			case 5:
				processed = sampleFive.clone();
				break;
			case 6:
				processed = sampleSix.clone();
				break;
			case 0:
				break;

			default:
				break;
			}

			for (int y = 0; y < processed.rows; y++)
			{
				for (int x = 0; x < processed.cols; x++)
				{
					int drawX = x + xPos;
					int drawY = y + yPos;

					if (drawX < visualfeedback.cols || drawY < visualfeedback.rows)
					{
						int B = processed.at<Vec3b>(y, x)[0];
						int G = processed.at<Vec3b>(y, x)[1];
						int R = processed.at<Vec3b>(y, x)[2];

						if (R == 255 && G == 0 && B == 128)
							continue;
						else
						{
							visualfeedback.at<Vec3b>(drawY, drawX)[0] = processed.at<Vec3b>(y, x)[0];
							visualfeedback.at<Vec3b>(drawY, drawX)[1] = processed.at<Vec3b>(y, x)[1];
							visualfeedback.at<Vec3b>(drawY, drawX)[2] = processed.at<Vec3b>(y, x)[2];
						}
					}
					else
						continue;
				}
			}
		}
	}
	imshow("Visual output", visualfeedback);
	waitKey(30);
}