#include "opencv2/opencv.hpp"
#include <cstdlib>
#include <iostream>
#include <windows.h>
#include <string>
#include <sstream>
#include "Camera.h"
#include "Shapes.h"
#include "Thresholding.h"

using namespace std;
using namespace cv;

Camera camera;
Thresholding thresholding;
Shapes shapes;
VideoCapture Cap(0);

int getInput()
{
	string stringtest;
	int choice;
	while (getline(cin, stringtest))
	{
		stringstream ss(stringtest);
		if (ss >> choice)
		{
			if (ss.eof())
			{return choice;} //Succes
		}
	}
	return 0; // Faliure
}

bool check()
{
	const int buffersize = 5;
	Mat buffer[buffersize];
	bool testing = true;
	bool same = true;
	while (testing == true)
	{
		//AQUIRE IMAGES
		for (int i = 0; i < buffersize; ++i)
		{
			Mat temp;
			camera.getImg(temp, Cap);
			cvtColor(temp, buffer[i], CV_BGR2GRAY);
		}

		//CHECK FOR DIFFERENCES
		bool checkImg = true;
		int errors = 0;
		for (int i = 0; i < buffersize && checkImg == true; ++i)
		{
			for (int j = 0; j < buffersize && checkImg == true; ++j)
			{
				for (int y = 0; y < buffer[i].rows; ++y)
				{
					for (int x = 0; x < buffer[i].cols; ++x)
					{
						int absVal = abs(buffer[i].at<uchar>(y, x) - buffer[j].at <uchar>(y, x));
						if (absVal < 30)
						{
							if (same == true)
								same = true;
						}
						else
						{
							same = false;
							++errors;
						}
					}
				}
			}
		}

		if (errors < 200)//LOOP EXIT
		{
			testing = false;
			return true;
		}
	}
}

void displayMainMenu()
{
	cout << "Main Menu\n";
	cout << endl;
	cout << "Please make your selection\n";
	cout << "1 - Run Program \n";
	cout << "2 - Save and calibrate shape\n";
	cout << "9 - Quit";
	cout << endl;
	cout << "Selection:";
}

void runProgram()
{
	bool stopProg = false;
	Mat inputImg;
	Mat thresImg;

	system("cls");
	cout << "Waiting for no movement\n";
	cout << "Press 'Esc' to quit\n";

	while (stopProg == false)
	{
		bool test;
		//CHECK FOR MOVEMENT
		test = check();
		//PROGRAM ENDER
		if (GetAsyncKeyState(VK_ESCAPE))
		{stopProg = true;}

		if (test = true)
		{
			//Get image from camera
			cout << "Attempting to retrieve image\n";
			camera.getImg(inputImg, Cap); // call get image function
			cout << "retrieved image\n";

			//Threshold image
			cout << "Attempting to threshold image\n";
			thresImg = inputImg;
			thresholding.thresColorImg(thresImg);
			cout << "Thresholded image\n";

			//Verify shape
			cout << "Attempted to recognize shapes\n";
			shapes.identifyShape(thresImg);
			cout << "Recognized and painted shapes\n";

			//Wait, clear console, reset to initial instructions on screen
			system("cls");
			cout << "Waiting for no movement\n";
			cout << "Press 'Esc' to quit\n";
		}

	}
}

void saveAndCalShape()
{
	bool stopProg = false;
	Mat inputImg;
	Mat thresImg;
	Mat shape;
	string fname;

	cout << "Attempting to retrieve image\n";
	camera.getImg(inputImg, Cap);

	cout << "retrieved image\n";
	cout << "Attempting to threshold image\n";
	thresImg = inputImg;
	thresholding.thresColorImg(thresImg);
	cout << "Thresholded image\n";

	cout << "Isolating image\n";
	shape = shapes.isolateBlob(thresImg , 255);
	cout << "Succesfully isolated image\n";
	imshow("Calibrated shape", shape);
	waitKey(30);

	system("cls");
	cout << "Please enter the file name of this shape (including .png)\n";
	cout << "if you do not want to save it write 'NULL'\n";
	cout << "Enter name:";
	getline(cin,fname);
	if (fname != "NULL")
	{
		imwrite(fname, shape);
	}
	destroyWindow("Calibrated shape");
}

int main()
{
	bool quit = false;
	int choice = 0;
	Mat input;
	Mat thres;

		do{
			system("cls");
			displayMainMenu();
			choice = getInput();

			switch (choice)
			{

			case 1:
				runProgram();
				break;

			case 2:
				system("cls");
				saveAndCalShape();
				break;

			case 9: // Quit program
				return 0;

			default:
				break;
			}

		} while (choice != 9);
		//DANIEL CAN YOU SEE!=!?
}
	