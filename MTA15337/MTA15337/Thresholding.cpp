#include "Thresholding.h"

using namespace std;
using namespace cv;

void Thresholding::thresColorImg(Mat &image)
{
	Mat Temp;

	if (image.data && !image.empty())
	{
		for (size_t y = 0; y < image.rows; ++y)
		{
			for (size_t x = 0; x < image.cols; ++x)
			{
				if (image.at<Vec3b>(y, x)[0] > 50)
				{
					image.at<Vec3b>(y, x)[0] = 255;
				}
				else
				{
					image.at<Vec3b>(y, x)[0] = 0;
				}

				if (image.at<Vec3b>(y, x)[1] > 50)
				{
					image.at<Vec3b>(y, x)[1] = 255;
				}
				else
				{ 
					image.at<Vec3b>(y, x)[1] = 0; 
				}

				if (image.at<Vec3b>(y, x)[2] > 50)
				{
					image.at<Vec3b>(y, x)[2] = 255;
				}
				else
				{ 
					image.at<Vec3b>(y, x)[2] = 0; 
				}

				if (image.at<Vec3b>(y, x)[0] == 255 && image.at<Vec3b>(y, x)[1] == 255 && image.at<Vec3b>(y, x)[2] == 255)
				{
					image.at<Vec3b>(y, x)[0] = 0;
					image.at<Vec3b>(y, x)[1] = 0;
					image.at<Vec3b>(y, x)[2] = 0;
				}

				if (image.at<Vec3b>(y, x)[0] > 0 || image.at<Vec3b>(y, x)[1] > 0 || image.at<Vec3b>(y, x)[2] > 0)
				{ 
					image.at<Vec3b>(y, x)[0] = 255;
					image.at<Vec3b>(y, x)[1] = 255;
					image.at<Vec3b>(y, x)[2] = 255;
				}
			}
		}

		cvtColor(image, Temp, CV_BGR2GRAY);
		Thresholding::EroDil(Temp);
		image = Temp;
	}
}

void Thresholding::EroDil(Mat &image)
{
	Mat erodeFilter = getStructuringElement(MORPH_RECT, Size(7, 7));;
	Mat erodeImage = image.clone();
	erode(image, erodeImage, erodeFilter);

	Mat dilationImage = erodeImage.clone();
	Mat dilationFilter = getStructuringElement(MORPH_RECT, Size(13, 13));
	dilate(erodeImage, dilationImage, dilationFilter, Point(-1, -1));
	image = dilationImage;
}