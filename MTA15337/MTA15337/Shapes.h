#pragma once
#include "opencv2\highgui.hpp"
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <cstdlib>
#include <windows.h>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <iterator>
#define _USE_MATH_DEFINES
#include <math.h>


using namespace std;
using namespace cv;

struct bstNode{
	int data;
	bstNode* left;
	bstNode* right;
};

struct sixD
{
	int w, h, a, p;
	float c, r;
};

class Shapes
{
public:
	void identifyShape(Mat &shape);
	Mat isolateBlob(Mat &image, int i);

private:
	//Algorithm functions
	int getW(int x1, int x2);
	int getH(int y1, int y2);
	float getC(float perimeter, int area);
	float getR(int i, int j);
	int getP(Mat shape);
	int getWidth(int x1, int x2);
	int getA(Mat blob);

	//Blob functions
	int findBlob(Mat &Img);

	//Calculate distance
	float getVectorDistance(vector<struct sixD> vector1, vector<struct sixD> vector2);
	int compareShape(vector<struct sixD> input);
	
	//Binary tree functions
	bstNode* insert(bstNode* root, int data);
	bstNode* getNewNode(int data);
	bool search(bstNode* root, int data);
	int findMin(bstNode* root);
};
