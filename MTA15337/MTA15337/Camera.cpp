#include "Camera.h"
using namespace std;
using namespace cv;

void Camera::getImg(Mat &input,VideoCapture cap)
{
	Mat inputImg = input;
	//CAPTURE IMAGE
	cap >> inputImg;

	//ROTATE IMG
	backwardsMapping(inputImg);

	//RETURN (pass by reference)
	 input = inputImg;
}

void Camera::backwardsMapping(Mat &image)
{
	Point2f cp(image.cols / 2, image.rows / 2);
	float angle = 180.0f;

	//Define corner points
	Point2i c[4];
	c[0] = Point2i(0, 0);
	c[1] = Point2i(image.cols - 1, 0);
	c[2] = Point2i(0, image.rows - 1);
	c[3] = Point2i(image.cols - 1, image.rows - 1);

	//rotate corners
	for (int i = 0; i < 4; i++){
		Point2i temp = Point2i(0, 0);
		temp.x = cp.x + cos(angle / 180 * M_PI)*(c[i].x - cp.x) - sin(angle / 180 * M_PI)*(c[i].y - cp.y);
		temp.y = cp.y + sin(angle / 180 * M_PI)*(c[i].x - cp.x) + cos(angle / 180 * M_PI)*(c[i].y - cp.y);
		c[i] = temp;
	}

	int minx = 10000, miny = 10000, maxx = 0, maxy = 0;

	for (int i = 0; i < 4; i++)
	{
		if (c[i].x < minx) 
			minx = c[i].x;
		if (c[i].y < miny) 
			miny = c[i].y;
		if (c[i].x > maxx) 
			maxx = c[i].x;
		if (c[i].y > maxy) 
			maxy = c[i].y;
	}

	//Calculate final image
	Mat finalImg = Mat::zeros(Size(maxx - minx, maxy - miny), CV_8UC3);
		if (image.data&& !image.empty())
		{
			for (int y = 0; y < finalImg.rows; y++)
				for (int x = 0; x < finalImg.cols; x++) 	//We are at (x,y) in the new image and then go through the old image to rotate the new image
				{
					float oX = cp.x + cos(angle / 180 * M_PI)*(x - (cp.x - minx)) - sin(angle / 180 * M_PI)*(y - (cp.y - miny)); //old X;
					float oY = cp.y + sin(angle / 180 * M_PI)*(x - (cp.x - minx)) + cos(angle / 180 * M_PI)*(y - (cp.y - miny)); //old Y

					// Ensure the image stays within the frame
					if (oX < 0 || oY < 0 || oX > image.cols - 1 || oY > image.rows - 1) 
						continue;

					//Interpolation
					int yL = floor(oY);
					int yH = ceil(oY);
					int xL = floor(oX);
					int xH = ceil(oX);

					//Calculate final image position based on interpolation
					finalImg.at<Vec3b>(y, x)[0] = (yH - oY)*(xH - oX)*image.at<Vec3b>(yL, xL)[0] + (yH - oY)*(1 - (xH - oX))*image.at<Vec3b>(yL, xH)[0] + (1 - (yH - oY))*(xH - oX)*image.at<Vec3b>(yH, xL)[0] + (1 - (yH - oY))*(1 - (xH -oX))*image.at<Vec3b>(yH, xH)[0];
					finalImg.at<Vec3b>(y, x)[1] = (yH - oY)*(xH - oX)*image.at<Vec3b>(yL, xL)[1] + (yH - oY)*(1 - (xH - oX))*image.at<Vec3b>(yL, xH)[1] + (1 - (yH - oY))*(xH - oX)*image.at<Vec3b>(yH, xL)[1] + (1 - (yH - oY))*(1 - (xH -oX))*image.at<Vec3b>(yH, xH)[1];
					finalImg.at<Vec3b>(y, x)[2] = (yH - oY)*(xH - oX)*image.at<Vec3b>(yL, xL)[2] + (yH - oY)*(1 - (xH - oX))*image.at<Vec3b>(yL, xH)[2] + (1 - (yH - oY))*(xH - oX)*image.at<Vec3b>(yH, xL)[2] + (1 - (yH - oY))*(1 - (xH - oX))*image.at<Vec3b>(yH, xH)[2];
				}
		}
		image = finalImg.clone();
}